from pygments.style import Style
from pygments.styles.lilypond import LilyPondStyle
from pygments.token import Generic


class LilyPondResultStyle(Style):
    styles = LilyPondStyle.styles.copy()
    styles[Generic.Output] = "#777"
    styles[Generic.Output.Marker] = ""
