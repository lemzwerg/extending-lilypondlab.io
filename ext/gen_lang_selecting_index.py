"""
This extension defines a custom builder that generates the toplevel
`index.html` file served at https://extending-lilypond.gitlab.io,
as opposed to the language-specific subdirectories.  This file
selects a language to redirect to according to the user's preferred
language.  It is created by rendering the `index.html` template.
"""

from pathlib import Path

from sphinx.builders.dummy import DummyBuilder

langs = Path("LINGUAS").read_text(encoding="utf-8").splitlines()


class LangSelectingIndexBuilder(DummyBuilder):
    name = "index"
    epilog = "Generated public/index.html"

    def init(self):
        self.create_template_bridge()
        self.templates.init(self)

    def get_outdated_dcos(self):
        return "public/index.html file"  # for logging

    def finish(self):
        ctx = {"langs": langs}
        output = self.templates.render("index.html", ctx)
        (Path(self.outdir) / "index.html").write_text(output, encoding="utf-8")


def setup(app):
    app.add_builder(LangSelectingIndexBuilder)
